## 仓库简介

华为云Serverless函数工作流FunctionGraph是一款带编排能力的函数计算服务，提供了界面化管理、一站式的函数开发上线功能，通过事件触发集成多种云服务，满足不同场景需求；根据请求的并发数量自动调度资源运行函数，实现按需极速弹性；函数运行实例出现异常，系统会启动新的实例处理后续的请求，实现秒级故障自愈。

#  项目总览

<table style="text-align: center">
    <tr style="font-weight: bold">
        <td>项目</td>
        <td>介绍</td>
        <td>仓库</td>
    </tr>
     <tr>
        <td rowspan="2">华为云functiongraph服务</td>
        <td>部署serverless函数到华为云函数工作流</td>
        <td><a href="https://gitee.com/HuaweiCloudDeveloper/deploy-functiongraph-workflow-sample">deploy-functiongraph-workflow-sample</a></td>
    </tr> 
    <tr>
        <td>github action</td>
        <td><a href="https://gitee.com/HuaweiCloudDeveloper/deploy-functiongraph-action">deploy-functiongraph-action</a></td>
      </tr>   
    <tr>
        <td rowspan="2">CAE serverless化运维</td>
        <td>体验使用CAE serverless化托管运维前端应用</td>
        <td><a href="https://gitee.com/HuaweiCloudDeveloper/huaweicloud-cae-frontend">huaweicloud-cae-frontend</a></td>
    </tr> 
    <tr>
        <td>体验使用CAE serverless化托管运维后端应用</td>
        <td><a href="https://gitee.com/HuaweiCloudDeveloper/huaweicloud-cae-backend">huaweicloud-cae-backend</a></td>
      </tr>
    <tr>
        <td rowspan="7">解决方案</td>
        <td>一键自动部署无服务器告警推送解决方案</td>
        <td><a href="https://gitee.com/HuaweiCloudDeveloper/huaweicloud-solution-serverless-alert-notifier">huaweicloud-solution-serverless-alert-notifier</a></td>
    </tr> 
    <tr>
        <td>定时变更实例规格解决方案</td>
        <td><a href="https://gitee.com/HuaweiCloudDeveloper/huaweicloud-solution-scheduled-changing-instances-specifications">huaweicloud-solution-scheduled-changing-instances-specifications</a></td>
      </tr> 
    <tr>
        <td>快速在华为云上生成图片缩略图</td>
        <td><a href="https://gitee.com/HuaweiCloudDeveloper/huaweicloud-solution-severless-thumbnails-generator">huaweicloud-solution-severless-thumbnails-generator</a></td>		
    </tr> 
    <tr>
        <td>在线解压对象存储服务(OBS)上的压缩包</td>
        <td><a href="https://gitee.com/HuaweiCloudDeveloper/huaweicloud-solution-serverless-file-decompression"> huaweicloud-solution-serverless-file-decompression</a></td>	
    <tr>
        <td>基于函数工作流快速构建云日志实时分析服务</td>
        <td><a href="https://gitee.com/HuaweiCloudDeveloper/huaweicloud-solution-serverless-real-time-log-analysis">huaweicloud-solution-serverless-real-time-log-analysis</a></td>		
    </tr>  	
    </tr>  	
    <tr>
        <td>低成本实现定制化视频处理能力</td>
        <td><a href="https://gitee.com/HuaweiCloudDeveloper/huaweicloud-solution-serverless-video-transcoding">huaweicloud-solution-serverless-video-transcoding</a></td>		
    </tr>  	
    </tr>   	
    <tr>
        <td>快速部署基于开源GraphQL的SQL to API方案</td>
        <td><a href="https://gitee.com/HuaweiCloudDeveloper/huaweicloud-solution-serverless-graphql-implementation">huaweicloud-solution-serverless-graphql-implementation</a></td>		
    </tr>  	
    </tr>  
    <tr>
        <td rowspan="2">项目演示</td>
        <td>基于serverless+华为云api技术的支持实践</td>
        <td><a href="https://gitee.com/HuaweiCloudDeveloper/huaweicloud-serverless-api-demos">huaweicloud-serverless-api-demos</a></td>
    </tr> 
    <tr>
        <td>通过华为云FunctionGraph实现抢红包场景</td>
        <td><a href="https://gitee.com/HuaweiCloudDeveloper/huaweicloud-serverless-function-graph-red-envelope-python-sample"> huaweicloud-Serverless-FunctionGraph-RedEnvelope-Python-Sample</a></td>		
    </tr>  
    <tr>
        <td rowspan="1">serverless介绍文档</td>
        <td>Serverles-FunctionGraph-RedEnvelope-Python示例代码</td>
        <td><a href="https://gitee.com/HuaweiCloudDeveloper/huaweicloud-Serverless-development-guide">huaweicloud-Serverless-development-guide</a></td>
    </tr> 
            </td>
</tr>
</table>






​        







